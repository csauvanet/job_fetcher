# Job Fetcher

Quick interview work - limit 3 h (readme written after the 3h deadline :-) )

## Quick start

Duplicate the `config\settings.json.default` into a `config\settings.json` file to put your own configuration.
Expected configuration:

```JSON
{
  "database": {
    "host": "mongodb://localhost:27017/job_fetcher"
  },
  "pole_emploi": {
    "id": "your application id",
    "secret": "your secret"
  }
}
```

To get `id` and `secret` you must register to https://www.emploi-store-dev.fr/portail-developpeur/catalogueapi (make an access request to API "Offres d'emploi Version 2")

Then simply install dependencies and start the app:

```Shell Session
npm i
npm start
```

Now simply browse http://localhost:3000/ 