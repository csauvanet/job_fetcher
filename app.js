var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var fs = require('fs')
var expressMongoDb = require('express-mongo-db')

var indexRouter = require('./routes/index')

var app = express()
var configFilePath = 'config/settings.json'
if (!fs.existsSync(configFilePath)) {
  console.error('You must provide your local ' + configFilePath + ' file to start the app.')
  process.exit(1)
}
var configFile = fs.readFileSync(configFilePath)
var jsonConfig = JSON.parse(configFile)

app.use(expressMongoDb(jsonConfig.database.host))
app.set('config', jsonConfig)

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/jquery', express.static(path.join(__dirname, '/node_modules/jquery/dist/')))

app.use('/', indexRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
