const request = require('request')
const tokenUrl = 'https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=partenaire'
const jobFetchUrl = 'https://api.emploi-store.fr/partenaire/offresdemploi/v2/offres/search?range=0-10&sort=1&departement=33'

// TODO give type to functions parameters
module.exports = {
  /**
   * gets a new token to use pole emploi API
   * @param {*} config object containing client secret and client id
   * @param {*} callback (error, token)
   */
  getNewToken: function (config, callback) {
    console.log('get new token')
    request.post(tokenUrl,
      {
        form: {
          grant_type: 'client_credentials',
          client_id: config.id,
          client_secret: config.secret,
          scope: 'api_offresdemploiv2 o2dsoffre application_' + config.id
        }
      }, function (err, httpResponse, body) {
        // TODO handle error
        if (err) {
          console.error(err)
          callback(err)
        } else {
          if (httpResponse.statusCode === 200) {
            let token = JSON.parse(body).access_token
            callback(null, token)
          } else {
            // other error
            console.error('Unexpected response: ' + httpResponse.statusCode)
            console.error(body.error)
            console.error(body.error_description)
            callback(new Error('Failed to get a valid token'))
          }
        }
      })
  },
  /**
   * gets latest job offer from API Offres d'emploi v2
   * @param {*} config object containing client secret and client id
   * @param {*} token token - if already known
   * @param {*} callback request callback (error, jobs)
   */
  getJobOffers: function (config, token, callback) {
    // TODO try first to reuse the previous token + request new token only on response 401
    this.getNewToken(config, function (err, token) {
      if (err) {
        console.error(err)
        callback(new Error('failed to get a valid token'))
      } else {
        request.get(jobFetchUrl, {
          headers: {
            'content-type': 'application/json',
            'accept': 'application/json'
          },
          'auth': {
            'bearer': token
          }
        }, function (err, httpResponse, body) {
          if (err) {
            console.error(err)
            callback(err)
          } else {
            if (httpResponse.statusCode === 200 || httpResponse.statusCode === 206) {
              let jsonBody = JSON.parse(body)
              if (jsonBody.resultats) {
                let jobsSummary = []
                jsonBody.resultats.forEach(element => {
                  jobsSummary.push({
                    id: element.id,
                    date: element.dateActualisation,
                    description: element.description,
                    entreprise: element.entreprise ? element.entreprise.nom : '',
                    intitule: element.intitule,
                    lieu: element.lieuTravail
                      ? element.lieuTravail.libelle + ' (' + element.lieuTravail.codePostal + ')' : '',
                    contrat: element.typeContrat
                  })
                })
                callback(null, jobsSummary)
              } else {
                callback(new Error('Unexpected empty response'))
              }
            } else {
              // other error
              console.error('Unexpected response: ' + httpResponse.statusCode)
              console.error(body.error)
              console.error(body.error_description)
              callback(new Error('Failed to fetch job offers'))
            }
          }
        })
      }
    })
  }
}
