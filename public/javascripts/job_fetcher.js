$('#upd_btn').click(function(){
  // TODO spinner while operation happens
  $('#processMsgArea').text('Processing...')
  $.post('/refresh', function(data, textStatus){
    if (data.success) {
      if (data.jobs) {
        $('#fetchTableResults tbody').empty()
        data.jobs.forEach(function (job) {
          var fullRow = '<tr>'
          fullRow += '<td>' + job.id + '</td>'
          fullRow += '<td>' + job.date + '</td>'
          fullRow += '<td>' + job.intitule + '</td>'
          fullRow += '<td>' + job.entreprise + '</td>'
          fullRow += '<td>' + job.description + '</td>'
          fullRow += '<td>' + job.lieu + '</td>'
          fullRow += '<td>' + job.contrat + '</td>'
          fullRow += '</tr>'
          $('#fetchTableResults tbody').append(fullRow)
        });
      } else {
        alert ('Error: no jobs in server response')
      }
    } else {
      alert ('Unable to get latest results: ' + data.error)
    }
    $('#processMsgArea').text('Processed !')
  })
})