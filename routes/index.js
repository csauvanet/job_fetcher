var express = require('express')
var router = express.Router()
var pea = require('../lib/poleEmploiApi')

/* GET home page. */
router.get('/', function (req, res, next) {
  req.db.collection('job_posts').find({}).toArray(function (err, result) {
    if (err) throw err
    // load those objects for the page
    res.locals.jobs = result
    next()
  })
}, function (req, res, next) {
  if (!res.locals.jobs || res.locals.jobs.length === 0) {
    // go fetch the jobs on pole-emploi
    pea.getJobOffers(req.app.get('config').pole_emploi, null, function (error, jobs) {
      if (error) {
        res.render('error', {
          message: 'Unable to connect to pole emploi and get job offers',
          error: error
        })
      } else {
        // write these data in DB
        req.db.collection('job_posts').insertMany(jobs, function (err, result) {
          if (err) {
            res.render('error', {
              message: 'Error while saving to DB',
              error: err
            })
          } else {
            res.locals.jobs = jobs
            next()
          }
        })
      }
    })
  } else {
    next()
  }
}, function (req, res, next) {
  res.render('index', {
    title: 'Job Fetcher',
    jobs: res.locals.jobs
  })
})

router.post('/refresh', function (req, res, next) {
  pea.getJobOffers(req.app.get('config').pole_emploi, null, function (error, jobs) {
    if (error) {
      res.jsonp({ success: false, error: error })
    } else {
      // replace DB content
      req.db.collection('job_posts').deleteMany({
        id: {
          $exists: true
        }
      }, function (err, obj) {
        if (err) {
          res.jsonp({ success: false, error: err.message })
        } else {
          // write new objects
          req.db.collection('job_posts').insertMany(jobs, function (err, result) {
            if (err) {
              res.jsonp({
                success: false,
                error: err
              })
            } else {
              res.jsonp({
                success: true,
                jobs: jobs
              })
            }
          })
        }
      })
    }
  })
})

module.exports = router
