var fs = require('fs')
var pea = require('../lib/poleEmploiApi')
var configFilePath = 'config/settings.json'

var configFile = fs.readFileSync(configFilePath)
var jsonConfig = JSON.parse(configFile)

pea.getJobOffers(jsonConfig.pole_emploi, null, function (err, jobs) {
  if (err) {
    console.error('Failed to get token')
    console.error(err)
  } else {
    jobs.forEach(element => {
      console.log(element.intitule + ' / ' + element.contrat + ' / ' + element.lieu)
    })
  }
})
