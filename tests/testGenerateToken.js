var fs = require('fs')
var pea = require('../lib/poleEmploiApi')
var configFilePath = 'config/settings.json'

var configFile = fs.readFileSync(configFilePath)
var jsonConfig = JSON.parse(configFile)

pea.getNewToken(jsonConfig.pole_emploi, function (err, token) {
  if (err) {
    console.error('Failed to get token')
    console.error(err)
  } else {
    console.log('Successfully got a token ' + token)
  }
})
