
let MongoClient = require('mongodb').MongoClient
let url = 'mongodb://localhost:27017/'

// username and password to be entered in the URL
MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
  if (err) throw err
  var dbo = db.db('job_fetcher')
  dbo.createCollection('job_posts', function (err, res) {
    if (err) throw err
    console.log('Collection created!')
    db.close()
  })
})
